// GotoDialog.cpp : file di implementazione
//

#include "stdafx.h"
#include "FileDissector.h"
#include "GotoDialog.h"


// finestra di dialogo CGotoDialog

IMPLEMENT_DYNAMIC(CGotoDialog, CDialog)

CGotoDialog::CGotoDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CGotoDialog::IDD, pParent)
{

}

CGotoDialog::~CGotoDialog()
{
}

void CGotoDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_gotoctrl);
}


BEGIN_MESSAGE_MAP(CGotoDialog, CDialog)
END_MESSAGE_MAP()

ULONGLONG CGotoDialog::GetSelectedAddress() const
{
	return _strtoui64 (CT2CA(m_gotoctrl), 0, 16);
}

void CGotoDialog::SetSelectedAddress(ULONGLONG addr)
{
	m_gotoctrl.Format (_T("%I64u"), addr);
}

