================================================================================
    LIBRERIA MFC: cenni preliminari sul progetto FileDissector
===============================================================================

La Creazione guidata applicazione ha creato questa applicazione 
FileDissector.  Questa applicazione non si limita a illustrare 
l'utilizzo di base delle classi MFC (Microsoft Foundation Class), ma 
costituisce anche un punto di partenza per la scrittura di un'applicazione.

Questo file contiene un riepilogo del contenuto di ciascun file che fa parte 
dell'applicazione FileDissector.

FileDissector.vcproj
    File di progetto principale per i progetti VC++ generati tramite una 
    Creazione guidata applicazione.
    Contiene informazioni sulla versione di Visual C++ che ha generato il file e
    informazioni sulle piattaforme, le configurazioni e le caratteristiche del 
    progetto selezionate con la Creazione guidata applicazione.

FileDissector.h
    File di intestazione principale dell'applicazione.  Include altre
    intestazioni specifiche del progetto (incluso il file Resource.h) e 
    dichiara la classe di applicazioni CFileDissectorApp.

FileDissector.cpp
    File di origine principale dell'applicazione che include la classe di
    applicazioni CFileDissectorApp.

FileDissector.rc
    Elenco di tutte le risorse Microsoft Windows utilizzate dal
    programma.  Include le icone, le bitmap e i cursori memorizzati
    nella sottodirectory RES.  Questo file pu� essere modificato direttamente
    in Microsoft Visual C++. Le risorse del progetto si trovano in 
    1040.

res\FileDissector.ico
    File di icona, utilizzato come icona dell'applicazione.  Questa
    icona � inclusa dal file di risorse principale FileDissector.rc.

res\FileDissector.rc2
    File contenente le risorse che non vengono modificate
    da Microsoft Visual C++. Inserire in questo file tutte le risorse non 
    modificabili dall'editor di risorse.

/////////////////////////////////////////////////////////////////////////////

Per la finestra frame principale:
    Il progetto include un'interfaccia MFC standard.

MainFrm.h, MainFrm.cpp
    File contenenti la classe frame CMainFrame, che viene 
    derivata da
    CMDIFrameWnd e controlla tutte le caratteristiche dei frame MDI.

res\Toolbar.bmp
    File bitmap utilizzato per creare icone affiancate per
    la barra degli strumenti.
    La barra degli strumenti e la barra di stato iniziali vengono costruite
    nella classe CMainFrame. Modificare la bitmap della barra 
    degli strumenti utilizzando l'editor di risorse e aggiornare la matrice 
    IDR_MAINFRAME TOOLBAR in FileDissector.rc per aggiungere
    i pulsanti della barra degli strumenti.
/////////////////////////////////////////////////////////////////////////////

Per la finestra frame figlio:

ChildFrm.h, ChildFrm.cpp
    File che definiscono e implementano la classe CChildFrame,
    che supporta le finestre figlio in un'applicazione MDI.

/////////////////////////////////////////////////////////////////////////////

Vengono creati automaticamente un tipo di documento e una visualizzazione:

FileDissectorDoc.h, FileDissectorDoc.cpp - il documento
    File contenenti la classe CFileDissectorDoc.  Modificare questi file
    per aggiungere i dati specifici per il proprio documento e implementare le 
    funzionalit� di salvataggio e caricamento dei file, utilizzando 
    CFileDissectorDoc::Serialize.

FileDissectorView.h, FileDissectorView.cpp - la visualizzazione del documento
    File contenenti la classe CFileDissectorView.
    Gli oggetti CFileDissectorView vengono utilizzati per visualizzare gli 
    oggetti CFileDissectorDoc.

res\FileDissectorDoc.ico
    File di icona, utilizzato come icona delle finestre figlio MDI per la
    classe CFileDissectorDoc.  Questa icona � inclusa dal file di risorse
    principale FileDissector.rc.




/////////////////////////////////////////////////////////////////////////////

Funzionalit� aggiuntive:

Controlli ActiveX
    L'applicazione include il supporto per l'utilizzo dei controlli ActiveX.

Supporto stampa e anteprima di stampa
    � stato generato automaticamente il codice per la gestione dei comandi di 
    stampa, impostazione della stampante e anteprima di stampa chiamando le 
    funzioni membro nella classe CView dalla libreria MFC.

/////////////////////////////////////////////////////////////////////////////

Altri file standard:

StdAfx.h, StdAfx.cpp
    File utilizzati per compilare un file di intestazione precompilato (PCH)
    denominato FileDissector.pch e un file dei tipi precompilato 
    denominato StdAfx.obj.

Resource.h
    File di intestazione standard che definisce i nuovi ID risorse.
    Questo file viene letto e aggiornato da Microsoft Visual C++.

FileDissector.manifest
	File manifesto dell'applicazione utilizzati da Windows XP per 
        descrivere la dipendenza di un'applicazione da versioni specifiche di 
        assembly side-by-side. Queste informazioni vengono utilizzate dal 
        caricatore per caricare l'assembly appropriato dall'Assembly Cache o
	l'assembly privato dall'applicazione. Il manifesto dell'applicazione 
        pu� essere incluso per la ridistribuzione come file manifesto esterno 
        installato nella stessa cartella dell'eseguibile dell'applicazione 
        oppure pu� essere incluso nell'eseguibile sotto forma di risorsa.
/////////////////////////////////////////////////////////////////////////////

Altre note:

Nella Creazione guidata applicazione viene utilizzato "TODO:" per indicare le 
parti del codice sorgente in cui � opportuno effettuare aggiunte o 
personalizzazioni.

Se l'applicazione utilizza MFC in una DLL condivisa, sar� necessario
ridistribuire le DLL MFC. Se la lingua dell'applicazione � diversa
dalla lingua delle impostazioni locali del sistema operativo, sar� necessario
ridistribuire anche le corrispondenti risorse localizzate MFC90XXX.DLL.
Per ulteriori informazioni su questi due argomenti, vedere la sezione sulla
ridistribuzione delle applicazioni Visual C++ nella documentazione MSDN.

/////////////////////////////////////////////////////////////////////////////
