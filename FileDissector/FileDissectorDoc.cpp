
// FileDissectorDoc.cpp : implementazione della classe CFileDissectorDoc
//

#include "stdafx.h"
#include "FileDissector.h"

#include "FileDissectorDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// --- Ordinamento Note --- //

bool CFileDissectorDoc::OrdinamentoNote::operator () (const CNota &n1, const CNota &n2)
{
	return n1.GetStartingAddr() < n2.GetStartingAddr();
}

// CFileDissectorDoc

IMPLEMENT_DYNCREATE(CFileDissectorDoc, CDocument)

BEGIN_MESSAGE_MAP(CFileDissectorDoc, CDocument)
END_MESSAGE_MAP()


// costruzione/eliminazione di CFileDissectorDoc

CFileDissectorDoc::CFileDissectorDoc()
{
	// TODO: aggiungere qui il codice di costruzione unico

}

CFileDissectorDoc::~CFileDissectorDoc()
{
}

BOOL CFileDissectorDoc::OnNewDocument()
{
	CFileDialog dlg (true);
	if (dlg.DoModal() == IDOK) {
		m_filename = dlg.GetPathName();
		if (m_file.Open (m_filename, CFile::modeRead | CFile::shareDenyNone) == 0) 
			return FALSE;
		return TRUE;
	}
	return FALSE;
}

// serializzazione di CFileDissectorDoc

void CFileDissectorDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		ar << m_filename;
		ar << (long)m_note.size();
		ContainerNote::const_iterator it = m_note.begin();
		while (it != m_note.end()) {
			ar << it->GetStartingAddr() << it->GetEndingAddr() << it->GetColor() << it->GetString();
			++it;
		}
	}
	else
	{
		long z;
		ar >> m_filename;
		ar >> z;
		m_note.clear();
		for (long i = 0; i < z; ++i) {
			ULONGLONG s,e;
			COLORREF c;
			CString str;

			ar >> s >> e >> c >> str;
			CNota nt (str, s, e);
			nt.SetColor (c);
			m_note.insert (nt);
		}
	}
}

#ifdef _DEBUG
void CFileDissectorDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CFileDissectorDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

void CFileDissectorDoc::AddNota (const CNota &nt)
{
	SetModifiedFlag();
	m_note.insert(nt);
}

ULONGLONG CFileDissectorDoc::GetData (ULONGLONG pos, void *buffer, ULONGLONG size)
{
	m_file.Seek (pos, CFile::begin);
	return m_file.Read (buffer, (UINT)size);
}

ULONGLONG CFileDissectorDoc::GetDataLen ()
{
	return m_file.GetLength();
}

CFileDissectorDoc::NotaIterator CFileDissectorDoc::GetNotaIterator()
{
	return CFileDissectorDoc::NotaIterator(m_note.begin());
}

const CNota &CFileDissectorDoc::GetNota (CFileDissectorDoc::NotaIterator &iterator) const
{
	return *iterator.iterator;
}

void CFileDissectorDoc::SetNota (NotaIterator &iterator, const CNota &nota)
{
	SetModifiedFlag();
	iterator = m_note.erase (iterator.iterator);
	m_note.insert (nota);
}

bool CFileDissectorDoc::IteratorAtEnd (CFileDissectorDoc::NotaIterator &iterator)
{
	return iterator.iterator == m_note.end();
}

unsigned int CFileDissectorDoc::GetSectionNumber() const
{
	return m_sezioni.size();
}

CFileDissectorDoc::SectionIterator CFileDissectorDoc::BeginSectionIterator() const
{
	return SectionIterator(m_sezioni.begin());
}

const CDataSection &CFileDissectorDoc::GetSection (const CFileDissectorDoc::SectionIterator &iterator) const
{
	return *iterator.iterator;
}

bool CFileDissectorDoc::IteratorAtEnd (const CFileDissectorDoc::SectionIterator &iterator) const
{
	return iterator.iterator == m_sezioni.end();
}


CFileDissectorDoc::NotaIterator CFileDissectorDoc::GetNotaIteratorFromAddress (ULONGLONG addr)
{
	CFileDissectorDoc::NotaIterator it = GetNotaIterator();
	while (!IteratorAtEnd(it)) {
		ULONGLONG sa = it.iterator->GetStartingAddr(), ea = it.iterator->GetEndingAddr();
		if ( sa <= addr && addr <= ea )
			return it;
		++it;
	}

	return it;
}

BOOL CFileDissectorDoc::OnOpenDocument(LPCTSTR lpszPathName)
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	if (m_file.Open (m_filename, CFile::modeRead | CFile::shareDenyNone) == 0) 
		return FALSE;

	ULONGLONG len = m_file.GetLength();

	m_sezioni.clear();
	m_sezioni.push_back (CDataSection (0, len-1));

	return TRUE;
}

void CFileDissectorDoc::ExportAsText (LPCTSTR fname, ULONGLONG from, ULONGLONG to, unsigned int linelength)
{
	CFile file;
	CFileException fileException;
	char blah[200];

	if (!file.Open(fname, CFile::modeWrite | CFile::modeCreate | CFile::shareExclusive , &fileException))
		AfxThrowFileException (fileException.m_cause);

	for (; from < to; from += linelength) {

		GetData (from, blah, linelength);

		CString line;
		CString ascii;
		CString hex;

		unsigned int maxl = (unsigned int) min ((ULONGLONG)linelength, (to - from));

		line.Format (_T("%08X "), from);
		for (unsigned int i = 0; i < linelength; ++i) {
			if (i < maxl)
				hex.Format(_T("%02X "), (unsigned char) blah[i]);
			else
				hex = "   ";
			line += hex;
		}
		for (unsigned int i = 0; i < maxl; ++i) {
			if (blah[i] < 32 || blah[i] > 126)
				line += ".";
			else
				line += blah[i];
		}
		line += "\r\n";

		file.Write (CT2CA(line), line.GetLength()); 
	}

	file.Close();
}
