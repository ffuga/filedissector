#pragma once

class CNota;

class CNoteDialog : public CDialog
{
	DECLARE_DYNAMIC(CNoteDialog)

public:
	CNoteDialog(const CNota &nota, CWnd *pParent = NULL);
	CNoteDialog(CWnd* pParent = NULL);   // costruttore standard
	virtual ~CNoteDialog();

// Dati della finestra di dialogo
	enum { IDD = IDD_ADDNOTEDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Supporto DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	COLORREF m_color;
	CString m_note;
	afx_msg void OnBnClickedButton2();
};
