#pragma once

class CDataSection
{
public:
	CDataSection(void);
	CDataSection (ULONGLONG start, ULONGLONG end);
	virtual ~CDataSection(void);

	ULONGLONG GetStart() const { return m_start; }
	ULONGLONG GetEnd() const { return m_end; }
	ULONGLONG GetLenght() const { return m_end - m_start; }

	static CDataSection Split (CDataSection &splitting, ULONGLONG at);

private:
	ULONGLONG m_start, m_end;
};
