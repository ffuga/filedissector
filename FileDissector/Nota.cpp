#include "StdAfx.h"
#include "Nota.h"

CNota::CNota(void)
: m_nota(), m_from(0), m_to(0), m_color(COLORREF(RGB(255,128,0)))
{
}

CNota::CNota (const CString &str, ULONGLONG from, ULONGLONG to)
: m_nota(str), m_from(from), m_to(to), m_color(COLORREF(RGB(255,128,0)))
{
}

CNota::~CNota(void)
{
}

ULONGLONG CNota::GetStartingAddr () const
{
	return m_from;
}

ULONGLONG CNota::GetEndingAddr () const
{
	return m_to;
}

CString CNota::GetString() const
{
	return m_nota;
}

COLORREF CNota::GetColor() const
{
	return m_color;
}

void CNota::SetColor (COLORREF col)
{
	m_color = col;
}

void CNota::SetText (const CString &str)
{
	m_nota = str;
}
