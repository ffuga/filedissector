#include "stdafx.h"
#include "FileDissector.h"

#include "FileDissectorDoc.h"
#include "FileDissectorView.h"
#include "MainFrm.h"
#include "NoteDialog.h"
#include "Nota.h"
#include "GotoDialog.h"

#include <limits>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

IMPLEMENT_DYNCREATE(CFileDissectorView, CScrollView)

BEGIN_MESSAGE_MAP(CFileDissectorView, CScrollView)
	ON_COMMAND(ID_FILE_PRINT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CFileDissectorView::OnFilePrintPreview)
	ON_WM_LBUTTONDOWN()
//	ON_WM_CHAR()
	ON_WM_KEYDOWN()
	ON_COMMAND(ID_MODIFICA_AGGIUNGINOTA, &CFileDissectorView::OnModificaAggiunginota)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_MODIFICA_VAIA, &CFileDissectorView::OnModificaVaia)
	ON_COMMAND(ID_FILE_ESPORTADUMP, &CFileDissectorView::OnFileEsportadump)
END_MESSAGE_MAP()

CFileDissectorView::CFileDissectorView()
: m_font (), m_byteperrow(16),
	possel(0),
	xsel(-1), ysel(-1),
	MouseStatus (NoneStatus)
{
	m_font.CreateFontW(0,0,0,0,0,0,0,0,0,0,0,0,0,_T("Courier New"));
}

CFileDissectorView::~CFileDissectorView()
{
}

BOOL CFileDissectorView::PreCreateWindow(CREATESTRUCT& cs)
{
	return CScrollView::PreCreateWindow(cs);
}


bool CFileDissectorView::GetNextNota (CFileDissectorDoc::NotaIterator &nextNota, CNota &nota) 
{
	CFileDissectorDoc *pDoc = GetDocument();
	if (!pDoc->IteratorAtEnd(nextNota)) {
		nota = pDoc->GetNota (nextNota);
		++nextNota;
		return true;
	}
	return false;
}

void CFileDissectorView::OnDraw(CDC* pDC)
{
	CFileDissectorDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	xborder = yborder = 30;

	CFont *oldfont = pDC->SelectObject (&m_font);
	long  x, y;
	x = xborder;
	y = yborder;
	ULONGLONG len = pDoc->GetDataLen();
	ULONGLONG pos = 0;
	char buffer[256];
	CRect rect;

	CSize csz = pDC->GetTextExtent (_T("M"));
	hchar = csz.cy;
	wchar = csz.cx;

	wborder = 1;
	// calcolo dimensionale 

	xaddrmin = xborder + wborder + wchar;
	xaddrmax = xaddrmin + 8 * wchar;
	xhexmin = xaddrmax + wchar;
	xhexmax = xhexmin + (m_byteperrow * 3 - 1) * wchar;
	xcharmin = xhexmax + wchar;
	xcharmax = xcharmin + m_byteperrow * wchar;
	xremarksmin = xcharmax + wchar;
	xremarksmax = xremarksmin + 32 * wchar;

	// 
	pDC->SetBkMode (TRANSPARENT);

	CBrush selected (COLORREF(RGB(255,255,0)));
	CBrush chselected (COLORREF(RGB(0,255,255)));

	CNota nota;
	bool HasNota;
	CFileDissectorDoc::NotaIterator nextNota = pDoc->GetNotaIterator();
	HasNota = GetNextNota (nextNota, nota);

	ULONGLONG xnota1, xnota2, ynota1, ynota2;
	ynota1 = nota.GetStartingAddr() / m_byteperrow;
	xnota1 = nota.GetStartingAddr() % m_byteperrow;
	ynota2 = nota.GetEndingAddr() / m_byteperrow;
	xnota2 = nota.GetEndingAddr() % m_byteperrow;

	int lnum = 0;
	int chnum;
	while (pos < len) {

		CRect clip;
		pDC->GetClipBox (&clip);
		if (y >= (clip.top - hchar) && y <= (clip.bottom + hchar)) {
			rect.left = x; rect.top = y;

			chnum = (int) pDoc->GetData (pos, buffer, m_byteperrow);

			CString txt;
			txt.Format (_T(" %08X "), pos);
			CString asciitxt;
			
			for (int i = 0; i < chnum; ++i) {
				CString h;
				h.Format (_T("%02X "), (unsigned char) buffer[i]);
				txt += h;
			}

			for (int i = 0; i < chnum; ++i) {
				char ch;
				if (buffer[i] < 32 || buffer[i] > 126) ch = '.';
				else ch = buffer[i];
				asciitxt += ch;
			}

			CSize sz = pDC->GetTextExtent (txt);
			rect.bottom = rect.top + sz.cy+1;
			rect.right = xremarksmax;

			pDC->Rectangle(&rect);

			if (lnum == linesel) {		// Disegna linea di selezione
				CRect r(rect);
				r.DeflateRect(1,1,1,1);
				pDC->FillRect(&r, &selected);
			}

			CString notatxt;
			if (HasNota) {				// Disegna evidenziazione e nota
				bool completed;
				do {
					completed = true;
					if (lnum >= ynota1 && lnum <= ynota2) {
						CRect notarect (rect);

						if (lnum == ynota1) {
							notarect.left = static_cast<LONG>(xhexmin + xnota1 * 3 * wchar);
							notatxt += nota.GetString();
						} else {
							notarect.left = xhexmin;
						}
						if (lnum == ynota2) {
							notarect.right = static_cast<LONG>(xhexmin + ((xnota2+1) * 3 ) * wchar);
						} else {
							notarect.right = xhexmax;
						}

						notarect.DeflateRect (0,1,0,1);
						CBrush brush (nota.GetColor());
						pDC->FillRect (&notarect, &brush);
					}
					if (lnum >= ynota2) {
						HasNota = GetNextNota (nextNota, nota);
						if (HasNota) {
							ynota1 = nota.GetStartingAddr() / m_byteperrow;
							xnota1 = nota.GetStartingAddr() % m_byteperrow;
							ynota2 = nota.GetEndingAddr() / m_byteperrow;
							xnota2 = nota.GetEndingAddr() % m_byteperrow;
							completed = false;
						}
					}
				} while (!completed);
			}

			if (lnum == linesel) {	// Disegna evidenziazione carattere corrente
				CRect chrect;
				if (chsel >= 0) {
					chrect.top = rect.top; chrect.bottom = rect.bottom;
					chrect.left = xhexmin + chsel * 3 * wchar;
					chrect.right = chrect.left + 2 * wchar;
					chrect.DeflateRect(1,1,1,1);
					pDC->FillRect(&chrect, &chselected);

					chrect.left = xcharmin + chsel * wchar; 
					chrect.right = chrect.left + wchar;
					chrect.DeflateRect(1,0,1,0);
					pDC->FillRect(&chrect, &chselected);
				}
			}

			if (MouseStatus == AddNoteStatus) {
				if (lnum == lineAdd) {
					CRect r;
					CPen pen (PS_SOLID, 5, COLORREF(RGB(0,0,128)));
					CPen *oldpen = pDC->SelectObject (&pen);

					r.top = rect.top; r.bottom = rect.bottom;
					r.left = xhexmin + chAdd * 3 * wchar;
					r.right = r.left + 2 * wchar;

					pDC->Rectangle(&r);

					pDC->SelectObject(oldpen);
				}
			}

			rect.bottom--;
			pDC->DrawText (txt,  rect, DT_SINGLELINE | DT_LEFT | DT_NOPREFIX);

			CRect hexrect(xcharmin, rect.top, xcharmax, rect.bottom);
			pDC->DrawText (asciitxt, hexrect, DT_SINGLELINE | DT_LEFT | DT_NOPREFIX);
			CRect notarect (xremarksmin, rect.top, xremarksmax, rect.bottom);
			pDC->DrawText (notatxt, notarect, DT_SINGLELINE | DT_LEFT | DT_NOPREFIX);

#if 0
	#ifdef _DEBUG
			CPen *oldpen;
			CPen pen (PS_SOLID, 2, COLORREF(RGB(255,0,0)));
			oldpen = pDC->SelectObject (&pen);

			pDC->MoveTo (xaddrmin, y); pDC->LineTo (xaddrmin,rect.bottom);
			pDC->MoveTo (xaddrmax, y); pDC->LineTo (xaddrmax,rect.bottom);
			pDC->MoveTo (xhexmin, y); pDC->LineTo (xhexmin,rect.bottom);
			pDC->MoveTo (xhexmax, y); pDC->LineTo (xhexmax,rect.bottom);
			pDC->MoveTo (xcharmin, y); pDC->LineTo (xcharmin,rect.bottom);
			pDC->MoveTo (xcharmax, y); pDC->LineTo (xcharmax,rect.bottom);
			pDC->MoveTo (xremarksmin, y); pDC->LineTo (xremarksmin,rect.bottom);
			pDC->MoveTo (xremarksmax, y); pDC->LineTo (xremarksmax,rect.bottom);

			pDC->SelectObject (oldpen);
	#endif
#endif

		} else {
			rect.bottom = y + hchar;
		}

		++lnum;
		y = rect.bottom;
		pos += m_byteperrow;
	}

	pDC->SelectObject(oldfont);
}

void CFileDissectorView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();

	CSize sizeTotal, sizePage, sizeLine;

	CDC *dc = GetDC();
	dc->SelectObject(m_font);
	CSize sz = dc->GetTextExtent (_T("M"));

	sizeTotal.cx = 30 + sz.cx * (16 + m_byteperrow * 4);
	ULONGLONG l = GetDocument()->GetDataLen();
	sizeTotal.cy = 60 + (LONG) (l / m_byteperrow * (sz.cy + 1));

	sizePage.cx = 15 * sz.cx;
	sizePage.cy = 15 * sz.cy;
	sizeLine.cx = sz.cx;
	sizeLine.cy = sz.cy;

	SetScrollSizes(MM_TEXT, sizeTotal, sizePage, sizeLine);
}

void CFileDissectorView::OnFilePrintPreview()
{
	AFXPrintPreview(this);
}

BOOL CFileDissectorView::OnPreparePrinting(CPrintInfo* pInfo)
{
	return DoPreparePrinting(pInfo);
}

void CFileDissectorView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
}

void CFileDissectorView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
}

void CFileDissectorView::OnRButtonUp(UINT nFlags, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CFileDissectorView::OnContextMenu(CWnd* pWnd, CPoint point)
{
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
}

#ifdef _DEBUG
void CFileDissectorView::AssertValid() const
{
	CScrollView::AssertValid();
}

void CFileDissectorView::Dump(CDumpContext& dc) const
{
	CScrollView::Dump(dc);
}

CFileDissectorDoc* CFileDissectorView::GetDocument() const // la versione non debug � inline.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CFileDissectorDoc)));
	return (CFileDissectorDoc*)m_pDocument;
}
#endif //_DEBUG

void CFileDissectorView::SelectAddress (ULONGLONG pos)
{
	possel = pos;
	chsel = possel % m_byteperrow;
	linesel = (unsigned int) (possel / m_byteperrow);
	xsel = ysel = -1;
	typesel = None;
}

void CFileDissectorView::ScrollToSelected()
{
	CPoint pt;
	pt.x = 0;
	pt.y = linesel * hchar + wborder + yborder;
	ScrollToPosition(pt);
}


void CFileDissectorView::SelectAddrFromPoint (const CPoint &point)
{
	CFileDissectorDoc *pDoc = GetDocument();

	CDC *dc = GetDC();
	OnPrepareDC(dc);

	CPoint mypoint (point);
	dc->DPtoLP (&mypoint);

	xsel = mypoint.x;
	ysel = mypoint.y;

	chsel = linesel = -1; 
	possel = 0;

	linesel = (mypoint.y - yborder - wborder) / (hchar);
	if (xsel >= xaddrmin && xsel < xaddrmax) {
		typesel = Addr;
	}
	if (xsel >= xhexmin && xsel < xhexmax) {
		typesel = Hex; 
		chsel = (xsel - xhexmin) / (3 * wchar);
	}
	if (xsel >= xcharmin && xsel < xcharmax) {
		typesel = Char;
		chsel = (xsel - xcharmin) / wchar;
	}

	if (chsel != -1 && linesel != -1) {
		possel = linesel * m_byteperrow + chsel;
	}

	CString txt;
	txt.Format (_T("Selezionato: x %d y %d; carattere %d; linea %d"), xsel, ysel, chsel, linesel);

	CMainFrame *frm = reinterpret_cast<CMainFrame *> (AfxGetMainWnd());
	frm->SetMessageText (txt);

	switch (MouseStatus) {
	case WaitFirstAddNoteStatus:
		chAdd = chsel;
		lineAdd = linesel;
		posAdd = possel;
		MouseStatus = AddNoteStatus;
		break;
	case AddNoteStatus:
		{
			CNoteDialog dlg;
			if (dlg.DoModal() == IDOK) {
				CNota nt(dlg.m_note, min(posAdd,possel), max(posAdd,possel));
				nt.SetColor (dlg.m_color);
				pDoc->AddNota (nt);
			}
			MouseStatus = NoneStatus;
		}
		break;
	default:
		break;
	}
}


void CFileDissectorView::OnLButtonDown(UINT nFlags, CPoint point)
{
	SelectAddrFromPoint (point);
	Invalidate();

	CScrollView::OnLButtonDown(nFlags, point);
}

void CFileDissectorView::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	switch (nChar) {
		case VK_DOWN:
			linesel++;
			break;
		case VK_UP:
			if (linesel > 0)
				linesel--;
			break;
		case VK_LEFT:
			if (chsel > 0) {
				--chsel;
			} else {
				if (linesel > 0) {
					chsel = m_byteperrow-1;
					--linesel;
				}
			}
			break;
		case VK_RIGHT:
			if (chsel < m_byteperrow-1) {
				++chsel;
			} else {
				chsel = 0;
				++linesel;
			}
			break;
		default:
			CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
			return;
	}

	Invalidate();
	CScrollView::OnKeyDown(nChar, nRepCnt, nFlags);
}

void CFileDissectorView::OnModificaAggiunginota()
{
	MouseStatus = WaitFirstAddNoteStatus;
}

void CFileDissectorView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	CFileDissectorDoc *pDoc = GetDocument();

	SelectAddrFromPoint (point);
	CFileDissectorDoc::NotaIterator nota_it = pDoc->GetNotaIteratorFromAddress(possel);

	if (!pDoc->IteratorAtEnd(nota_it)) {
		CNota nota = pDoc->GetNota(nota_it);
		CNoteDialog dlg (nota);
		if (dlg.DoModal()) {
			nota.SetColor (dlg.m_color);
			nota.SetText (dlg.m_note);
			pDoc->SetNota (nota_it, nota);

			Invalidate();
		}
	}

	CScrollView::OnLButtonDblClk(nFlags, point);
}

void CFileDissectorView::OnModificaVaia()
{
	CGotoDialog dlg;

	dlg.SetSelectedAddress (possel);
	if (dlg.DoModal() == IDOK) {
		possel = dlg.GetSelectedAddress();
		SelectAddress(possel);
		ScrollToSelected();
	}
}

void CFileDissectorView::OnFileEsportadump()
{
	CFileDialog dlg (false, _T(".txt"));

	if (dlg.DoModal() == IDOK) {
		GetDocument()->ExportAsText (dlg.GetPathName(), 0, GetDocument()->GetDataLen(), m_byteperrow);
	}
}
