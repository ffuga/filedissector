#pragma once
#include "afxwin.h"

class CGotoDialog : public CDialog
{
	DECLARE_DYNAMIC(CGotoDialog)

public:
	CGotoDialog(CWnd* pParent = NULL);   
	virtual ~CGotoDialog();

	enum { IDD = IDD_GOTODIALOG };

	ULONGLONG GetSelectedAddress() const;
	void SetSelectedAddress(ULONGLONG addr);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);

	DECLARE_MESSAGE_MAP()
	CString m_gotoctrl;
};
