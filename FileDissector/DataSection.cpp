#include "StdAfx.h"
#include "DataSection.h"

CDataSection::CDataSection(void)
: m_start(0), m_end(0)
{
}

CDataSection::CDataSection (ULONGLONG start, ULONGLONG end)
: m_start(start), m_end(end)
{
}

CDataSection::~CDataSection(void)
{
}

CDataSection CDataSection::Split (CDataSection &splitting, ULONGLONG at)
{
	CDataSection splitted (at, splitting.GetEnd());
	splitting.m_end = at-1;

	return splitted;
}
