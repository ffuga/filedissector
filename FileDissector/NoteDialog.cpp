// NoteDialog.cpp : file di implementazione
//

#include "stdafx.h"
#include "FileDissector.h"
#include "NoteDialog.h"
#include "Nota.h"

// finestra di dialogo CNoteDialog

IMPLEMENT_DYNAMIC(CNoteDialog, CDialog)

CNoteDialog::CNoteDialog(const CNota &nota, CWnd *pParent)
: CDialog (CNoteDialog::IDD, pParent),
	m_note(nota.GetString()),
	m_color(nota.GetColor())
{
}


CNoteDialog::CNoteDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CNoteDialog::IDD, pParent)
	, m_color (COLORREF(RGB(255,128,0)))
	, m_note(_T(""))
{

}

CNoteDialog::~CNoteDialog()
{
}

void CNoteDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_note);
}


BEGIN_MESSAGE_MAP(CNoteDialog, CDialog)
	ON_BN_CLICKED(IDC_BUTTON2, &CNoteDialog::OnBnClickedButton2)
END_MESSAGE_MAP()


// gestori di messaggi CNoteDialog

void CNoteDialog::OnBnClickedButton2()
{
	CColorDialog dlg(m_color,CC_FULLOPEN);

	if (dlg.DoModal() == IDOK) 
		m_color = dlg.GetColor();
}
