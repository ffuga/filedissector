#pragma once


class CFileDissectorView : public CScrollView
{
protected:
	CFileDissectorView();
	DECLARE_DYNCREATE(CFileDissectorView)

public:
	CFileDissectorDoc* GetDocument() const;
	virtual ~CFileDissectorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	virtual void OnDraw(CDC* pDC); 
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	virtual void OnInitialUpdate(); 
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

	void SelectAddrFromPoint (const CPoint &pt);

	void SelectAddress (ULONGLONG pos);
	void ScrollToSelected();
private:
	CFont m_font;

	unsigned int m_byteperrow;

	// Coordinate e dimensioni elementi grafici

	long xaddrmin, xaddrmax;				// Posizione x indirizzo
	long xhexmin, xhexmax;					// Posizione x hex
	long xcharmin, xcharmax;				// Posizione caratteri
	long xremarksmin, xremarksmax;			// Posizione note

	long xborder, yborder;
	long wborder;							// larghezza bordo in pixel
	long hchar, wchar;						// altezza caratteri

	// Selezione

	int xsel, ysel;				// selezione, caratteri grafici
	unsigned int chsel, linesel;			// selezione, caratteri e linee
	ULONGLONG possel;						// Selezione, Posizione
	enum { None, Addr, Hex, Char, Remarks } typesel;

	// Aggiunta note e stato

	int chAdd, lineAdd;			// Posizione Marker Aggiunta note
	ULONGLONG posAdd;
	enum { NoneStatus, WaitFirstAddNoteStatus, AddNoteStatus } MouseStatus;

	// 

	bool GetNextNota (CFileDissectorDoc::NotaIterator &nextNota, CNota &nota);

protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
//	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnModificaAggiunginota();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnModificaVaia();
	afx_msg void OnFileEsportadump();
};

#ifndef _DEBUG  // versione di debug in FileDissectorView.cpp
inline CFileDissectorDoc* CFileDissectorView::GetDocument() const
   { return reinterpret_cast<CFileDissectorDoc*>(m_pDocument); }
#endif

