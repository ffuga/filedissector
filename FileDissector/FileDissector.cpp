
// FileDissector.cpp : definisce i comportamenti delle classi per l'applicazione.
//

#include "stdafx.h"
#include "afxwinappex.h"
#include "FileDissector.h"
#include "MainFrm.h"

#include "ChildFrm.h"
#include "FileDissectorDoc.h"
#include "FileDissectorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFileDissectorApp

BEGIN_MESSAGE_MAP(CFileDissectorApp, CWinAppEx)
	ON_COMMAND(ID_APP_ABOUT, &CFileDissectorApp::OnAppAbout)
	// Comandi di documenti basati su file standard
	ON_COMMAND(ID_FILE_NEW, &CWinAppEx::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, &CWinAppEx::OnFileOpen)
	// Comando di stampa standard
	ON_COMMAND(ID_FILE_PRINT_SETUP, &CWinAppEx::OnFilePrintSetup)
END_MESSAGE_MAP()


// costruzione di CFileDissectorApp

CFileDissectorApp::CFileDissectorApp()
{

	m_bHiColorIcons = TRUE;

	// TODO: inserire qui il codice di costruzione.
	// Inserire l'inizializzazione significativa in InitInstance.
}

// L'unico e solo oggetto CFileDissectorApp

CFileDissectorApp theApp;


// Inizializzazione di CFileDissectorApp

BOOL CFileDissectorApp::InitInstance()
{
	// InitCommonControlsEx() � necessario in Windows XP se nel manifesto
	// di un'applicazione � specificato l'utilizzo di ComCtl32.dll versione 6 o successiva per attivare
	// gli stili visuali. In caso contrario, non sar� possibile creare finestre.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Effettuare questa impostazione in modo da includere tutte le classi di controlli comuni da utilizzare
	// nell'applicazione.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinAppEx::InitInstance();

	// Inizializzare le librerie OLE.
	if (!AfxOleInit())
	{
		AfxMessageBox(IDP_OLE_INIT_FAILED);
		return FALSE;
	}
	AfxEnableControlContainer();
	// Inizializzazione standard
	// Se non si utilizzano queste funzionalit� e si desidera ridurre la dimensione
	// dell'eseguibile finale, � necessario rimuovere dal seguente codice
	// le specifiche routine di inizializzazione che non sono necessarie.
	// Modificare la chiave del Registro di sistema in cui sono memorizzate le impostazioni
	// TODO: � necessario modificare questa stringa in modo appropriato,
	// inserendo ad esempio il nome della societ� o dell'organizzazione.
	SetRegistryKey(_T("StudioFuga"));
	LoadStdProfileSettings(4);  // Caricare le opzioni del file INI standard (inclusa MRU).

	InitContextMenuManager();

	InitKeyboardManager();

	InitTooltipManager();
	CMFCToolTipInfo ttParams;
	ttParams.m_bVislManagerTheme = TRUE;
	theApp.GetTooltipManager()->SetTooltipParams(AFX_TOOLTIP_TYPE_ALL,
		RUNTIME_CLASS(CMFCToolTipCtrl), &ttParams);

	// Registrare i modelli di documenti dell'applicazione. I modelli di documenti
	//  funzionano da connessione tra documenti, finestre cornice e viste.
	CMultiDocTemplate* pDocTemplate;
	pDocTemplate = new CMultiDocTemplate(IDR_FileDissectorTYPE,
		RUNTIME_CLASS(CFileDissectorDoc),
		RUNTIME_CLASS(CChildFrame), // frame MDI figlio personalizzato
		RUNTIME_CLASS(CFileDissectorView));
	if (!pDocTemplate)
		return FALSE;
	AddDocTemplate(pDocTemplate);

	// creare finestra cornice MDI principale.
	CMainFrame* pMainFrame = new CMainFrame;
	if (!pMainFrame || !pMainFrame->LoadFrame(IDR_MAINFRAME))
	{
		delete pMainFrame;
		return FALSE;
	}
	m_pMainWnd = pMainFrame;
	// richiamare DragAcceptFiles solo se � presente un suffisso.
	//  In un'applicazione MDI questo deve verificarsi immediatamente dopo l'impostazione di m_pMainWnd.


	// Analizzare la riga di comando per i comandi shell standard, DDE, apri file
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);


	// Invia i comandi specificati nella riga di comando. Restituisce FALSE se
	// l'applicazione � stata avviata con l'opzione /RegServer, /Register, /Unregserver o /Unregister.
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;
	// La finestra principale � stata inizializzata, quindi � possibile visualizzarla e aggiornarla.
	pMainFrame->ShowWindow(m_nCmdShow);
	pMainFrame->UpdateWindow();

	return TRUE;
}



// finestra di dialogo CAboutDlg utilizzata per visualizzare le informazioni sull'applicazione.

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dati della finestra di dialogo
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // supporto DDX/DDV

// Implementazione
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()

// comando dell'applicazione per eseguire la finestra di dialogo
void CFileDissectorApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

// Metodi di caricamento/salvataggio personalizzazione di CFileDissectorApp

void CFileDissectorApp::PreLoadState()
{
	BOOL bNameValid;
	CString strName;
	bNameValid = strName.LoadString(IDS_EDIT_MENU);
	ASSERT(bNameValid);
	GetContextMenuManager()->AddMenu(strName, IDR_POPUP_EDIT);
}

void CFileDissectorApp::LoadCustomState()
{
}

void CFileDissectorApp::SaveCustomState()
{
}

// gestori dei messaggi di CFileDissectorApp



