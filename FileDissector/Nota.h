#pragma once

#ifndef ULONGLONG
typedef unsigned __int64 ULONGLONG;
#endif

class CNota
{
public:
	CNota(void);
	CNota (const CString &str, ULONGLONG from, ULONGLONG to);
	virtual ~CNota(void);

	ULONGLONG GetStartingAddr () const;
	ULONGLONG GetEndingAddr () const;
	CString GetString() const;
	COLORREF GetColor() const;

	void SetText (const CString &str);
	void SetColor (COLORREF col);

private:
	CString m_nota;
	ULONGLONG m_from, m_to;

	COLORREF m_color;
};
