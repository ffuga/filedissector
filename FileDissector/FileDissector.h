
// FileDissector.h : file di intestazione principale per l'applicazione FileDissector
//
#pragma once

#ifndef __AFXWIN_H__
	#error "includere 'stdafx.h' prima di includere questo file per PCH"
#endif

#include "resource.h"       // simboli principali


// CFileDissectorApp:
// Vedere FileDissector.cpp per l'implementazione di questa classe
//

class CFileDissectorApp : public CWinAppEx
{
public:
	CFileDissectorApp();


// Override
public:
	virtual BOOL InitInstance();

// Implementazione
	UINT  m_nAppLook;
	BOOL  m_bHiColorIcons;

	virtual void PreLoadState();
	virtual void LoadCustomState();
	virtual void SaveCustomState();

	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CFileDissectorApp theApp;
