#pragma once

#include <set>
#include <list>
#include "Nota.h"
#include "DataSection.h"

class CFileDissectorDoc : public CDocument
{
private:
	class OrdinamentoNote {
	public:
		bool operator () (const CNota &n1, const CNota &n2);
	};

	typedef std::set<CNota, OrdinamentoNote> ContainerNote;

	class OrdinamentoSezioni {
	public:
		bool operator () (const CDataSection &s1, const CDataSection &s2);
	};

	typedef std::list<CDataSection> ContainerSection;

protected:
	CFileDissectorDoc();
	DECLARE_DYNCREATE(CFileDissectorDoc)

public:
	virtual ~CFileDissectorDoc();

	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);

	ULONGLONG GetData (ULONGLONG pos, void *buffer, ULONGLONG size);
	ULONGLONG GetDataLen () ;

	void ExportAsText (LPCTSTR fname, ULONGLONG from, ULONGLONG to, unsigned int linelength);

// Note

	template <typename T>
	class Iterator {
		friend CFileDissectorDoc;
	public:
		Iterator (const Iterator<T> &altra) 
			: iterator (altra.iterator) {}
		~Iterator() {}

		Iterator<T> &operator ++() {
			++iterator; return *this; 
		}
	private:
		Iterator() :iterator() {}
		Iterator(typename T::const_iterator it) 
			: iterator (it) {}
		typename T::const_iterator iterator;
	};

	typedef Iterator<ContainerNote> NotaIterator;
	typedef Iterator<ContainerSection> SectionIterator;

	void AddNota (const CNota &nt);
	NotaIterator GetNotaIterator();
	const CNota &GetNota (NotaIterator &iterator) const;
	void SetNota (NotaIterator &iterator, const CNota &nota);
	bool IteratorAtEnd (NotaIterator &iterator);

	NotaIterator GetNotaIteratorFromAddress (ULONGLONG addr);

/// --- Accesso alle sezioni

	unsigned int GetSectionNumber() const;
	SectionIterator BeginSectionIterator() const;
	const CDataSection &GetSection (const SectionIterator &iterator) const;
	bool IteratorAtEnd (const SectionIterator &iterator) const;


#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

private:

	CFile m_file;
	CString m_filename;
	ContainerNote m_note;
	ContainerSection m_sezioni;

protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
};


